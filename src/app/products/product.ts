export interface Product {
    name: string;
    emailaddress: string;
    phonenumber: string;
}