import { Product } from './product';

export const PRODUCT_ITEMS: Product[] = [{
    name: 'John Doe',
    emailaddress: 'john.doe@facebook.com',
    phonenumber: '0837249033'
}, {
    name: 'Jane Doe',
    emailaddress: 'jane.doe@yahoo.com',
    phonenumber: '0837249033'
}, {
    name: 'Josh Doe',
    emailaddress: 'josh.doe@gmail.com',
    phonenumber: '0837249033'
}]